@SpringBootApplication 
@RestController 
public class CalcServiceApplication {
  public static final String OPERATION_URL = "http://MICROCALC-%1s-OPERATION/{param}";
  public static final String TRANSLATE_URL = "http://MICROCALC-TRANSLATE-SERVICE/{param}";
  private RestTemplate restTemplate = new RestTemplate();

  public static void main(String[] args) {
    SpringApplication.run(CalcServiceApplication.class, args);
  }
  
  @RequestMapping("/{alias}/{params:.+}") 
  public ResponseEntity<BigDecimal> calc(@PathVariable String alias, 
                                         @PathVariable BigDecimal... params) {

    String operation = restTemplate.getForEntity(TRANSLATE_URL, String.class, alias).getBody();
    
    String paramsStr = Arrays.stream(params)
          .map(n -> n.toPlainString().trim())
          .collect(Collectors.joining(","));
    
    return 
        ResponseEntity
          .ok(restTemplate.getForEntity(
                  String.format(OPERATION_URL, operation)
                  , BigDecimal.class
                  , paramsStr
                )
             )
          .getBody();
  }
  
}

