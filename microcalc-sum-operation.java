@SpringBootApplication 
@RestController 
public class SumOperationController {

  public static void main(String[] args) {
    SpringApplication.run(SumOperationController.class, args);
  }
  
  @RequestMapping("/{params:.+}")
  public ResponseEntity<BigDecimal> sum(@PathVariable("params") List<BigDecimal> params) {

    Optional<BigDecimal> result = params.stream().reduce((a, b) -> a.add(b));
    
    if (result.isPresent()) {
      return ResponseEntity.ok(result.get());
    }
    
    return ResponseEntity.badRequest().body(null);
  }

}

